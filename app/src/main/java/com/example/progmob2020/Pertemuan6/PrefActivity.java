package com.example.progmob2020.Pertemuan6;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.progmob2020.R;


public class PrefActivity extends AppCompatActivity {
    String isLogin = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pref);

        Button btnPref = (Button)findViewById(R.id.btnPref);
        final EditText username = findViewById(R.id.txtUsername);
        EditText password = findViewById(R.id.txtPassword);

        SharedPreferences pref = PrefActivity.this.getSharedPreferences("prefs_file",MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();


        isLogin = pref.getString("isLogin","0");
        if (isLogin.equals("1")){
            btnPref.setText("Logout");
        }else{
            btnPref.setText("Login");
        }



        btnPref.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isLogin = pref.getString("isLogin",null);
                if ((username.getText().toString().contains("72180233"))&&(password.getText().toString().contains("12345678"))){
                    editor.putString("isLogin","Mahasiswa");
                    Toast.makeText(PrefActivity.this,"Berhasil Login", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(PrefActivity.this, HomeActivity.class);
                    startActivity(intent);
                }else{
                    Toast.makeText(PrefActivity.this,"NIM atau Password Salah !", Toast.LENGTH_LONG).show();
                    btnPref.setEnabled(false);
                    Intent intent = new Intent(PrefActivity.this, PrefActivity.class);
                    startActivity(intent);
                }
                editor.commit();
            }
        });
    }
}