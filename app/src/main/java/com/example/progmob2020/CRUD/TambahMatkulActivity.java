package com.example.progmob2020.CRUD;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.progmob2020.Network.GetDataService;
import com.example.progmob2020.Network.RetrofitClientInstance;
import com.example.progmob2020.R;
import com.example.progmob2020.model.DefaultResult;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class TambahMatkulActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_matkul);
        EditText NamaMatkul = (EditText)findViewById(R.id.txtNamaAddMatkul);
        EditText KodeMatkul = (EditText)findViewById(R.id.txtKodeAddMatkul);
        EditText HariMatkul = (EditText)findViewById(R.id.txtHariAddMatkul);
        EditText SesiMatkul = (EditText)findViewById(R.id.txtSesiAddMatkul);
        EditText SksMatkul = (EditText)findViewById(R.id.txtSksAddMatkul);
        Button btnTambahMkSimpan = (Button)findViewById(R.id.btnSimpanAddMatkul);
        pd = new ProgressDialog(TambahMatkulActivity.this);

        btnTambahMkSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Tunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.add_matkul(
                        NamaMatkul.getText().toString(),
                        "72180233",
                        KodeMatkul.getText().toString(),
                        Integer.parseInt(HariMatkul.getText().toString()),
                        Integer.parseInt(SesiMatkul.getText().toString()),
                        Integer.parseInt(SksMatkul.getText().toString())
                );
                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(TambahMatkulActivity.this,"Berhasil", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(TambahMatkulActivity.this,DaftarMatkulActivity.class);
                        startActivity(intent);
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(TambahMatkulActivity.this,"Gagal", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}