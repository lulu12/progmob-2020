package com.example.progmob2020.CRUD;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.progmob2020.Network.GetDataService;
import com.example.progmob2020.Network.RetrofitClientInstance;
import com.example.progmob2020.R;
import com.example.progmob2020.model.DefaultResult;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class UbahMatkulActivity extends AppCompatActivity {
    ProgressDialog pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ubah_matkul);
        EditText edKodeLama = (EditText)findViewById(R.id.edKodeLamaMatkul);
        EditText edNamaBaru = (EditText)findViewById(R.id.edNamaUbahMatkul);
        EditText edKodeBaru = (EditText)findViewById(R.id.edKodeBaruMatkul);
        EditText edHariBaru = (EditText)findViewById(R.id.edHariBaruMatkul);
        EditText edSesiBaru = (EditText)findViewById(R.id.edSesiBaruMatkul);
        EditText edSksBaru = (EditText)findViewById(R.id.edSksBaruMatkul);
        Button btnSimpanUbah = (Button)findViewById(R.id.btnSimpanUbahMatkul);
        pd = new ProgressDialog(UbahMatkulActivity.this);

        btnSimpanUbah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Tunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.update_matkul(
                        edNamaBaru.getText().toString(),
                        "72180233",
                        edKodeBaru.getText().toString(),
                        Integer.parseInt(edHariBaru.getText().toString()),
                        Integer.parseInt(edSesiBaru.getText().toString()),
                        Integer.parseInt(edSksBaru.getText().toString()),
                        edKodeLama.getText().toString()
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(UbahMatkulActivity.this,"Data Berhasil di Update",Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(UbahMatkulActivity.this,"Gagal di Update",Toast.LENGTH_LONG).show();
                    }
                });

                Intent intent = new Intent(UbahMatkulActivity.this,MainMatkulActivity.class);
                startActivity(intent);


            }
        });
    }
}