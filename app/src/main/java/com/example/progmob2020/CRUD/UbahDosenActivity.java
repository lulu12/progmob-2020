package com.example.progmob2020.CRUD;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.progmob2020.Network.GetDataService;
import com.example.progmob2020.Network.RetrofitClientInstance;
import com.example.progmob2020.R;
import com.example.progmob2020.model.DefaultResult;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class UbahDosenActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ubah_dosen);
        EditText edNidnLama = (EditText)findViewById(R.id.edNidnLama);
        EditText edNamaBaru = (EditText)findViewById(R.id.edNamaBaruDosen);
        EditText edNidnBaru = (EditText)findViewById(R.id.edNidnBaruDosen);
        EditText edAlamatBaru = (EditText)findViewById(R.id.edAlamatBaruDosen);
        EditText edEmailBaru = (EditText)findViewById(R.id.edEmailBaruDosen);
        EditText edGelarBaru = (EditText)findViewById(R.id.edGelarBaruDosen);
        Button btnSimpanUbah = (Button)findViewById(R.id.btnSimpanUbahDosen);
        pd = new ProgressDialog(UbahDosenActivity.this);

        btnSimpanUbah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.update_dosen(
                        edNamaBaru.getText().toString(),
                        edNidnBaru.getText().toString(),
                        edAlamatBaru.getText().toString(),
                        edEmailBaru.getText().toString(),
                        edGelarBaru.getText().toString(),
                        "",
                        "72180233",
                        edNidnLama.getText().toString()

                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(UbahDosenActivity.this,"Data Berhasil di Update",Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(UbahDosenActivity.this,"Gagal di Update",Toast.LENGTH_LONG).show();
                    }
                });

                Intent intent = new Intent(UbahDosenActivity.this,MainDosenActivity.class);
                startActivity(intent);
            }
        });
    }
}