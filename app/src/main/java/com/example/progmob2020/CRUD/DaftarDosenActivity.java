package com.example.progmob2020.CRUD;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.example.progmob2020.Adapter.DosenCRUDRecyclerAdapter;
import com.example.progmob2020.Network.GetDataService;
import com.example.progmob2020.Network.RetrofitClientInstance;
import com.example.progmob2020.R;
import com.example.progmob2020.model.DosenTTS;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DaftarDosenActivity extends AppCompatActivity {
    RecyclerView rvDosen;
    DosenCRUDRecyclerAdapter dosenAdapter;
    ProgressDialog pd;
    List<DosenTTS> dosenList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_dosen);

        rvDosen = (RecyclerView)findViewById(R.id.rvDosen);
        pd = new ProgressDialog(this);
        pd.setTitle("Mohon Tunggu");
        pd.show();

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<DosenTTS>> call = service.getDosen("72180233");

        call.enqueue(new Callback<List<DosenTTS>>() {
            @Override
            public void onResponse(Call<List<DosenTTS>> call, Response<List<DosenTTS>> response) {
                pd.dismiss();
                dosenList = response.body();
                dosenAdapter = new DosenCRUDRecyclerAdapter(dosenList);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(DaftarDosenActivity.this);
                rvDosen.setLayoutManager(layoutManager);
                rvDosen.setAdapter(dosenAdapter);
            }

            @Override
            public void onFailure(Call<List<DosenTTS>> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(DaftarDosenActivity.this,"Error",Toast.LENGTH_LONG).show();

            }
        });

    }
}