package com.example.progmob2020.CRUD;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.example.progmob2020.CRUD.DaftarDosenActivity;
import com.example.progmob2020.CRUD.HapusDosenActivity;
import com.example.progmob2020.CRUD.TambahDosenActivity;
import com.example.progmob2020.CRUD.UbahDosenActivity;
import com.example.progmob2020.R;


public class MainDosenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_dosen);

        Button btnTambahDosen = (Button)findViewById(R.id.btnTambahDosen);
        Button btnDataDosen = (Button)findViewById(R.id.btnDataDosen);
        Button btnUbahDosen = (Button)findViewById(R.id.btnUbahDosen);
        Button btnHapusDosen = (Button)findViewById(R.id.btnHapusDosen);

        //action
        btnDataDosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainDosenActivity.this, DaftarDosenActivity.class);
                startActivity(intent);
            }
        });
        btnTambahDosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainDosenActivity.this, TambahDosenActivity.class);
                startActivity(intent);
            }
        });
        btnUbahDosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainDosenActivity.this, UbahDosenActivity.class);
                startActivity(intent);
            }
        });
        btnHapusDosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainDosenActivity.this, HapusDosenActivity.class);
                startActivity(intent);
            }
        });

    }
}