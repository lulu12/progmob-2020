package com.example.progmob2020.CRUD;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.progmob2020.Network.GetDataService;
import com.example.progmob2020.Network.RetrofitClientInstance;
import com.example.progmob2020.R;
import com.example.progmob2020.model.DefaultResult;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TambahMahasiswaActivity extends AppCompatActivity {

    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_mahasiswa);

        EditText edNama = (EditText)findViewById(R.id.editTextNama);
        EditText edNim = (EditText)findViewById(R.id.editTextNim);
        EditText edAlamat = (EditText)findViewById(R.id.editTextAlamat);
        EditText edEmail = (EditText)findViewById(R.id.editTextEmail);
        Button btnSimpan = (Button)findViewById(R.id.buttonSimpanMhs);
        pd = new ProgressDialog(TambahMahasiswaActivity.this);

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Loading");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.add_mhs(
                        edNama.getText().toString(),
                        edNim.getText().toString(),
                        edAlamat.getText().toString(),
                        edEmail.getText().toString(),
                        "",
                        "72180233"
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(TambahMahasiswaActivity.this,"Berhasil",Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(TambahMahasiswaActivity.this,LihatMahasiswaActivity.class);
                        startActivity(intent);
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        Toast.makeText(TambahMahasiswaActivity.this,"Gagal", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}