package com.example.progmob2020.CRUD;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.example.progmob2020.Adapter.MatkulCRUDRecyclerAdapter;
import com.example.progmob2020.Network.GetDataService;
import com.example.progmob2020.Network.RetrofitClientInstance;
import com.example.progmob2020.R;
import com.example.progmob2020.model.MatkulTTS;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DaftarMatkulActivity extends AppCompatActivity {
    RecyclerView rvMatkul;
    MatkulCRUDRecyclerAdapter matkulAdapter;
    ProgressDialog pd;
    List<MatkulTTS> matakuliahList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_matkul);
        rvMatkul = (RecyclerView)findViewById(R.id.rvMatkul);
        pd = new ProgressDialog(this);
        pd.setTitle("Mohon Tunggu");
        pd.show();

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<MatkulTTS>> call = service.getMatakuliah("72180233");

        call.enqueue(new Callback<List<MatkulTTS>>() {
            @Override
            public void onResponse(Call<List<MatkulTTS>> call, Response<List<MatkulTTS>> response) {
                pd.dismiss();
                matakuliahList = response.body();
                matkulAdapter = new MatkulCRUDRecyclerAdapter(matakuliahList);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(DaftarMatkulActivity.this);
                rvMatkul.setLayoutManager(layoutManager);
                rvMatkul.setAdapter(matkulAdapter);

            }

            @Override
            public void onFailure(Call<List<MatkulTTS>> call, Throwable t) {
                Toast.makeText(DaftarMatkulActivity.this,"ERROR", Toast.LENGTH_LONG);
            }
        });
    }
}