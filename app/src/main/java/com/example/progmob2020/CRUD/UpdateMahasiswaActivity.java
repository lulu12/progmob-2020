package com.example.progmob2020.CRUD;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.progmob2020.Network.GetDataService;
import com.example.progmob2020.Network.RetrofitClientInstance;
import com.example.progmob2020.R;
import com.example.progmob2020.model.DefaultResult;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateMahasiswaActivity extends AppCompatActivity {

    EditText editTextNimCari, editTextNama, editTextNim, editTextAlamat, editTextEmail;
    Button btnUbah;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_mahasiswa);

        editTextNimCari = (EditText)findViewById(R.id.editTextNimCari);
        editTextNama = (EditText)findViewById(R.id.editTextNamaBaru);
        editTextNim = (EditText)findViewById(R.id.editTextNimBaru);
        editTextAlamat = (EditText)findViewById(R.id.editTextAlamatBaru);
        editTextEmail = (EditText)findViewById(R.id.editTextEmailBaru);
        btnUbah = (Button)findViewById(R.id.buttonUbah);
        pd = new ProgressDialog(UpdateMahasiswaActivity.this);

        Intent data = getIntent();
        editTextNimCari.setText(data.getStringExtra("nim"));
        editTextNama.setText(data.getStringExtra("nama"));
        editTextNim.setText(data.getStringExtra("nim"));
        editTextAlamat.setText(data.getStringExtra("alamat"));
        editTextEmail.setText(data.getStringExtra("email"));

        btnUbah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Silahkan Tunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance. getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.update_mhs(
                        editTextNama.getText().toString(),
                        editTextNim.getText().toString(),
                        editTextNimCari.getText().toString(),
                        editTextAlamat.getText().toString(),
                        editTextEmail.getText().toString(),
                        "Fotonya Diubah",
                        "72180233"
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(UpdateMahasiswaActivity.this,"Data Berhasil di Update",Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(UpdateMahasiswaActivity.this,"Gagal di Update",Toast.LENGTH_LONG).show();
                    }
                });

                Intent intent = new Intent(UpdateMahasiswaActivity.this,MainMhsActivity.class);
                startActivity(intent);
            }
        });
    }
}