package com.example.progmob2020.Network;



import com.example.progmob2020.model.DefaultResult;
import com.example.progmob2020.model.DosenTTS;
import com.example.progmob2020.model.MahasiswaCRUD;
import com.example.progmob2020.model.MatkulTTS;

import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface GetDataService {

    //Mahasiswa
    @GET("api/progmob/mhs/{nim_progmob}")
    Call<List<MahasiswaCRUD>> getMahasiswa(@Path("nim_progmob") String nim_progmob);

    @FormUrlEncoded
    @POST("api/progmob/mhs/delete")
    Call<DefaultResult>delete_mhs(
            @Field("nim") String id,
            @Field("nim_progmob") String nim_progmob
    );
    @FormUrlEncoded
    @POST("api/progmob/mhs/create")
    Call<DefaultResult> add_mhs(
            @Field("nama") String nama,
            @Field("nim") String nim,
            @Field("alamat") String alamat,
            @Field("email") String email,
            @Field("foto") String foto,
            @Field("nim_progmob") String nim_progmob
    );
    @FormUrlEncoded
    @POST("api/progmob/mhs/update")
    Call<DefaultResult>update_mhs(
            @Field("nama") String nama,
            @Field("nim") String nim,
            @Field("nim_cari") String nim_cari,
            @Field("alamat") String alamat,
            @Field("email") String email,
            @Field("foto") String foto,
            @Field("nim_progmob") String nim_progmob
    );

    //Dosen
    @GET("api/progmob/dosen/{nim_progmob}")
    Call<List<DosenTTS>> getDosen(@Path("nim_progmob") String nim_progmob);

    @FormUrlEncoded
    @POST("api/progmob/dosen/create")
    Call<DefaultResult>add_dosen(
            @Field("nama") String nama,
            @Field("nidn") String nidn,
            @Field("alamat") String alamat,
            @Field("email") String email,
            @Field("gelar") String gelar,
            @Field("foto") String foto,
            @Field("nim_progmob") String nim_progmob
    );

    @FormUrlEncoded
    @POST("api/progmob/dosen/delete")
    Call<DefaultResult>delete_dosen(
            @Field("nidn") String id,
            @Field("nim_progmob") String nim_progmob
    );

    @FormUrlEncoded
    @POST("api/progmob/dosen/update")
    Call<DefaultResult>update_dosen(
            @Field("nama") String nama,
            @Field("nidn") String nidn,
            @Field("alamat") String alamat,
            @Field("email") String email,
            @Field("gelar") String gelar,
            @Field("foto") String foto,
            @Field("nim_progmob") String nim_progmob,
            @Field("nidn_cari") String nidn_cari
    );

    //Matakuliah
    @GET("api/progmob/matkul/{nim_progmob}")
    Call<List<MatkulTTS>> getMatakuliah(@Path("nim_progmob") String nim_progmob);

    @FormUrlEncoded
    @POST("api/progmob/matkul/create")
    Call<DefaultResult>add_matkul(
            @Field("nama") String nama,
            @Field("nim_progmob") String nim_progmob,
            @Field("kode") String kode,
            @Field("hari") int hari,
            @Field("sesi") int sesi,
            @Field("sks") int sks
    );

    @FormUrlEncoded
    @POST("api/progmob/matkul/update")
    Call<DefaultResult>update_matkul(
            @Field("nama") String nama,
            @Field("nim_progmob") String nim_progmob,
            @Field("kode") String kode,
            @Field("hari") int hari,
            @Field("sesi") int sesi,
            @Field("sks") int sks,
            @Field("kode_cari") String kode_cari
    );

    @FormUrlEncoded
    @POST("api/progmob/matkul/delete")
    Call<DefaultResult>delete_matkul(
            @Field("kode") String id,
            @Field("nim_progmob") String nim_progmob
    );
}