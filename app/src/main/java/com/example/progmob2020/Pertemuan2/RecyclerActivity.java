package com.example.progmob2020.Pertemuan2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.progmob2020.Adapter.MahasiswaRecyclerAdapter;
import com.example.progmob2020.R;
import com.example.progmob2020.model.Mahasiswa;


import java.util.ArrayList;
import java.util.List;

public class RecyclerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);

        RecyclerView rv = (RecyclerView)findViewById(R.id.rvLatihan);

        MahasiswaRecyclerAdapter mahasiswaRecyclerAdapter;

        //Datanya
        List<Mahasiswa> mahasiswaList = new ArrayList<Mahasiswa>();

        //generate data mahasiswa
        Mahasiswa m1 = new Mahasiswa("72180233", "Louis Halawa", "081264690902");
        Mahasiswa m2 = new Mahasiswa("72180234", "X", "081264690903");
        Mahasiswa m3 = new Mahasiswa("72180235", "Y", "081264690904");
        Mahasiswa m4 = new Mahasiswa("72180236", "Z", "081264690905");
        Mahasiswa m5 = new Mahasiswa("72180237", "H", "081264690906");
        Mahasiswa m6 = new Mahasiswa("72180238", "I", "081264690907");
        Mahasiswa m7 = new Mahasiswa("72180239", "J", "081264690908");

        mahasiswaList.add(m1);
        mahasiswaList.add(m2);
        mahasiswaList.add(m3);
        mahasiswaList.add(m4);
        mahasiswaList.add(m5);
        mahasiswaList.add(m6);
        mahasiswaList.add(m7);

        mahasiswaRecyclerAdapter = new MahasiswaRecyclerAdapter(RecyclerActivity.this);
        mahasiswaRecyclerAdapter.setMahasiswaList(mahasiswaList);

        rv.setLayoutManager(new LinearLayoutManager(RecyclerActivity.this));
        rv.setAdapter(mahasiswaRecyclerAdapter);

    }
}