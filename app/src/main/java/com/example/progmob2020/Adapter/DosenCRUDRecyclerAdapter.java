package com.example.progmob2020.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.example.progmob2020.R;
import com.example.progmob2020.model.DosenTTS;

import java.util.ArrayList;
import java.util.List;

public class DosenCRUDRecyclerAdapter extends RecyclerView.Adapter<DosenCRUDRecyclerAdapter.ViewHolder>{
    private Context context;
    private List<DosenTTS> dosenList;

    public DosenCRUDRecyclerAdapter(Context context){
        this.context = context;
        dosenList = new ArrayList<>();
    }

    public DosenCRUDRecyclerAdapter(List<DosenTTS> dosenList){
        this.dosenList = dosenList;
    }

    public List<DosenTTS> getDosenList() {
        return dosenList;
    }

    public void setDosenList(List<DosenTTS> dosenList) {
        this.dosenList = dosenList;
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_dosen,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position){
        DosenTTS d = dosenList.get(position);

        holder.tvNamaD.setText(d.getNama());
        holder.tvNidnD.setText(d.getNidn());
        holder.tvAlamatD.setText(d.getAlamat());
        holder.tvEmailD.setText(d.getEmail());
        holder.tvGelarD.setText(d.getGelar());

    }

    @Override
    public int getItemCount(){
        return dosenList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvNamaD;
        public TextView tvNidnD;
        public TextView tvAlamatD;
        public TextView tvEmailD;
        public TextView tvGelarD;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvNamaD = itemView.findViewById(R.id.tvNamaD);
            tvNidnD = itemView.findViewById(R.id.tvNidnD);
            tvAlamatD = itemView.findViewById(R.id.tvAlamatD);
            tvEmailD = itemView.findViewById(R.id.tvEmailD);
            tvGelarD = itemView.findViewById(R.id.tvGelarD);
        }
    }
}
