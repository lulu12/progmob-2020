package com.example.progmob2020.model;

public class Mahasiswa {
    private String nim;
    private String nama;
    private String noTel;

    public Mahasiswa(String nim, String nama, String noTel) {
        this.nim = nim;
        this.nama = nama;
        this.noTel = noTel;


    }

    public String getNim() {
        return nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNoTel() {
        return noTel;
    }

    public void setNoTel(String noTel) {
        this.noTel = noTel;
    }
}
